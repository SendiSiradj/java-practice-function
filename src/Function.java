public class Function {

    /* 1.static dan non static
     pengembalian nilai dan tanpa pengembalian nilai
     2. Abstract
     3. Lamdha
     5. Rekursif


     */

    /*int hitung(int nilaiAwal) {


        return 0;
    }*/

    static int factorial(int angka){
        int hasil;
        if (angka == 0){
            return 1;
        }
        else {
            hasil = angka * factorial(angka - 1);
            return hasil;
        }
    }

    public static void main(String[] args) {
        int result = factorial(4);
        System.out.println(result);
    }
}
