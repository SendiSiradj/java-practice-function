public class Lamdha {

    interface OperationAritmatik {

        int add(int a, int b);
    }

    private int operasi(int a, int b, OperationAritmatik operasi){
        return operasi.add(a, b);
    }

    interface OperationTemperature {

        int x(int suhuCelcius);
    }
    private int tempOperation(int suhuCelcius, OperationTemperature operasi){
        return operasi.x(suhuCelcius);
    }

    public static void main(String[] args) {

        /*OperationAritmatik tambah = (int a, int b) -> a + b;
        OperationAritmatik kurang = (int a, int b) -> a - b;
        OperationAritmatik kali = (int a, int b) -> a * b;
        OperationAritmatik bagi = (int a, int b) -> a / b;

        Lamdha lamdha = new Lamdha();

        int hasil = lamdha.operasi(50, 30, tambah);
        System.out.println(hasil);*/

        OperationTemperature Fahrenheit = (int suhuCelcius) -> ((9 * suhuCelcius)/5) + 32;
        OperationTemperature Kelvin = (int suhuCelcius) -> suhuCelcius + 273;

        Lamdha lamdha = new Lamdha();
        int hasilFahreneheit = lamdha.tempOperation(30, Fahrenheit);
        System.out.println(hasilFahreneheit);
        int hasilKelvin = lamdha.tempOperation(30, Kelvin);
        System.out.println(hasilKelvin);

    }
}
