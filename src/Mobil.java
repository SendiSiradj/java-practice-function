public interface Mobil {

    void openDoor(String message);
    void insertKey(String message);
}

class Tesla implements Mobil {
    @Override
    public void openDoor(String check) {
        if (check == "true") {
            String message = "Pintu mobil Tesla telah dibuka";
            System.out.println(message);
        }
    }

    @Override
    public void insertKey(String check) {
        if (check == "true") {
            String message = "Kunci mobil Tesla telah terpasang";
            System.out.println(message);
        }
    }
}

class Avanza implements Mobil {
    @Override
    public void openDoor(String check) {
        if (check == "true") {
            String message = "Pintu mobil Avanza telah dibuka";
            System.out.println(message);
        }
    }

    @Override
    public void insertKey(String check) {
        if (check == "true") {
            String message = "Kunci mobil Avanza telah terpasang";
            System.out.println(message);
        }
    }
}

class programRunner {
    public static void main(String[] args) {
        Tesla mobilTesla = new Tesla();
        mobilTesla.insertKey("true");
        mobilTesla.openDoor("true");

        Avanza mobilAvanza = new Avanza();
        mobilAvanza.insertKey("true");
        mobilAvanza.openDoor("true");
    }
}
